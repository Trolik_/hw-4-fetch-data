"use strict";

const createUL = document.createElement('ul');
createUL.setAttribute('id', 'films-list');
document.body.appendChild(createUL);

const filmsList = document.getElementById("films-list");


fetch('https://ajax.test-danit.com/api/swapi/films')
	.then(response => response.json())
	.then(items => {
		items.forEach((StarWarsEpisode) => {
			const filmEpisode = document.createElement('li')

			filmEpisode.insertAdjacentHTML(
				'beforeend',
				`<p> <b>EpisodeId - </b>${StarWarsEpisode.episodeId}, </p>
                 <p id="NameOfEpisode"> <b>Name - </b>"${StarWarsEpisode.name}"</p>
                 <p> <b>Opening Crawl - </b>${StarWarsEpisode.openingCrawl}</p>`
			)

			const filmName = filmEpisode.querySelector('#NameOfEpisode')

			const charactersTitle = document.createElement('p')
			charactersTitle.classList.add('filmCharacters')
			charactersTitle.innerHTML = '<b>Characters :</b>'

			charactersList(StarWarsEpisode.characters)
				.then(data => {
					const charactersNames = data.map(character => character.name)

					const charactersList = document.createElement('ul')
					charactersList.classList.add('charactersList')
					charactersList.innerHTML = charactersNames.map(name => `<li>${name}</li>`).join('')

					charactersTitle.append(charactersList)
					filmName.after(charactersTitle)
				})

			filmsList.append(filmEpisode)
		})
	})
	.catch(error => postMessage.innerHTML = "Error: " + error + ".")


function charactersList(urls) {
	const requests = urls.map(url => fetch(url)
		.then(data => data.json()))
	return Promise.all(requests)
}
